/*

��� ����� �������

��� ��� std::unordered_set ��������� ��� ������
����������, ���� �� ��������� ���� � ����������,
�� ����� ������������ ��� ��� ����������
���������� �����.

�������� ������-���������, ����������� �� ����
std::vector<int>, � ������� ���������� �����,
������� ����� �����������. ������ ������
�������� unordered_set � ��� ������ ����
�����������, ������� ��� ������� ����� ���
����������� ������. � ���������� �������
std::unique_ptr<std::vector<int>>, ����������
����� ��� ��������. ��� ������� �� ����������
����������� ����� ������ ����� for � ��������
����� auto.

*/

#include <iostream>
#include <unordered_set>

int main()
{
    auto unique_nums = [](std::vector<int> vec) {
        // make full-set from in-vector
        std::unordered_set<int> set;
        for (auto v : vec) {
            set.insert(v);
        }
        int counter{ 0 };
        std::vector<int>out;
        // sorting doubles
        for (auto s : set) {
            for (auto v : vec) {
                if (s == v) {
                    ++counter;
                }
            }
            if (counter == 1) {
                out.push_back(s);
            }
            counter = 0;
        }
        //  print unique numbers
        for (auto o : out) {
            std::cout << o << "\n";
        }
        // return smart pointer
        std::unique_ptr<std::vector<int>>out_unique = std::make_unique<std::vector<int>>(out);
        return out_unique;
    };

    std::vector<int> vec{ 1,2,2,3,3,3,4,4,4,4,5,7,9 };
    // using lambda 
    unique_nums(vec);
}

